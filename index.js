var nodes = [
    {
        id: 'nonExistent',
        title: 'nonExistent',
        comment: 'Заявка еще не создана, только формируется'
    }, {
        id: 'waitsConsultant',
        title: 'waitsConsultant'
    }, {
        id: 'appointedConsultant',
        title: 'appointedConsultant'
    }, {
        id: 'applied',
        title: 'applied'
    },  {
        id: 'inWork',
        title: 'inWork'
    },  {
        id: 'dispatcherDenied',
        title: 'dispatcherDenied'
    },  {
        id: 'consultantDenied',
        title: 'consultantDenied'
    },  {
        id: 'described',
        title: 'described'
    },  {
        id: 'closedSuccessfully',
        title: 'closedSuccessfully'
    },  {
        id: 'closedWithDeny',
        title: 'closedWithDeny'
    }, {
        id: 'additionalConsultation',
        title: 'additionalConsultation'
    }, {
        id: 'expertisePositive',
        title: 'expertisePositive'
    }, {
        id: 'expertiseNegative',
        title: 'expertiseNegative'
    }, {
        id: 'onExpertise',
        title: 'onExpertise'
    }
];

var edges = [
    {
        from: 'nonExistent',
        to: 'waitsConsultant',
        label: "create study"
    }, {
        from: 'nonExistent',
        to: 'appointedConsultant',
        label: "appoint to consultant"
    }, {
        from: 'appointedConsultant',
        to: 'applied',
        label: "create study"
    }, {
        from: 'waitsConsultant',
        to: 'appointedConsultant',
        label: "create study"
    },
    {
        from: 'applied',
        to: 'inWork',
        label: "create study"
    },
    {
        from: 'inWork',
        to: 'described',
        label: "create study"
    },
    {
        from: 'inWork',
        to: 'appointedConsultant',
        label: "create study"
    },
    {
        from: 'applied',
        to: 'consultantDenied',
        label: "create study"
    },
    {
        from: 'waitsConsultant',
        to: 'dispatcherDenied',
        label: "create study"
    },
    {
        from: 'appointedConsultant',
        to: 'consultantDenied',
        label: "create study"
    },
    {
        from: 'consultantDenied',
        to: 'appointedConsultant',
        label: "create study"
    },
    {
        from: 'consultantDenied',
        to: 'dispatcherDenied',
        label: "create study"
    },
    {
        from: 'dispatcherDenied',
        to: 'closedWithDeny',
        label: "create study"
    },
    {
        from: 'dispatcherDenied',
        to: 'appointedConsultant',
        label: "create study"
    },
    {
        from: 'described',
        to: 'closedSuccessfully',
        label: "create study"
    },
    {
        from: 'described',
        to: 'appointedConsultant',
        label: "create study"
    },
    {
        from: 'inWork',
        to: 'additionalConsultation',
        label: "create study"
    },
    {
        from: 'additionalConsultation',
        to: 'described',
        label: "create study"
    },
    {
        from: 'additionalConsultation',
        to: 'additionalConsultation',
        label: "create study"
    },
    {
        from: 'additionalConsultation',
        to: 'appointedConsultant',
        label: "create study"
    },
    {
        from: 'closedSuccessfully',
        to: 'onExpertise',
        label: "create study"
    },
    {
        from: 'onExpertise',
        to: 'expertisePositive',
        label: "create study"
    },
    {
        from: 'onExpertise',
        to: 'expertiseNegative',
        label: "create study"
    }
];

var container = document.getElementById('app');

var data= {
    nodes: nodes,
    edges: edges
};

var options = {
    physics: {
        barnesHut: {
            enabled: true,
            gravitationalConstant: -10000,
            centralGravity: 0,
            springLength: 300
        }
    },
    edges: {
        style: 'arrow'
    },
    hover: true,
    navigation: true,
    keyboard: true,
    dataManipulation: true,
    width: '100%',
    height: '700px'
};
var network = new vis.Network(container, data, options);

network.on('select', function(elements) {
    var isEdgesSelect = elements.edges.length > 0;
    var isNodesSelect = elements.nodes.length > 0;
    var isElementsSelect = isEdgesSelect && isNodesSelect;

    var allEdges = this.edges;
    var allNodes = this.nodes;

    if (isElementsSelect) {

        var selectedNodeId = elements.nodes[0];
        var selectedNodeEdgesId = elements.edges;

        selectedNodeEdgesId.forEach(function(id){
            var currentEdge = allEdges[id];
            var isFromEdge = currentEdge.fromId === selectedNodeId;
            var isToEdge = currentEdge.toId === selectedNodeId;

            if (isFromEdge) {
                currentEdge.options.color.highlight = "red";
            }

            if (isToEdge) {
                currentEdge.options.color.highlight = "green";
            }
        });

    }
});